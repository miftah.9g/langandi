@extends('adminlte::page')

@section('title', 'Langandi:Daftar Pengeluaran')

@section('content')
<div class="row col-md-offset-2 col-md-8">
    @if(session('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
        {{ session('success') }}
    </div>

    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pengeluaran</h3>
            <form action="{{ route('expense.index') }}" method="get" class="pull-right">
                <label for="category">Kategori: </label>
                <select name="category" id="year">
                    <option value="">-- Pilih --</option>
                    @foreach($categories as $category)
                    <option value="{{ $category }}" {{ $category == $categoryFromInput ? 'selected' : '' }}>{{ ucfirst($category) }}</option>
                    @endforeach
                </select>
                <label for="year">Tahun: </label>
                <select name="year" id="year">
                    <option value="">-- Pilih --</option>
                    @for($year = $currentYear; $year >= $farthestYear; $year--)
                    <option value="{{ $year }}" {{ $year == $yearFromInput ? 'selected' : '' }}>{{ $year }}</option>
                    @endfor
                </select>
                <label for="month">Bulan: </label>
                <select name="month" id="month">
                    <option value="">-- Pilih --</option>
                    @for($month = 1; $month <= 12; $month++)
                    <option value="{{ $month }}" {{ $month == $monthFromInput ? 'selected' : '' }}>{{ $month }}</option>
                    @endfor
                </select>
                <button class="btn btn-info">Filter</button> 
            </form>
        </div>
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Tanggal</th>
                        <th>Kategori</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Action</th>
                    </tr>
                    @foreach($expenses as $expense)
                    <tr>
                        <td>{{ $expense->date->toDateString() }}</td>
                        <td>{{ $expense->category ? ucfirst($expense->category) : '-' }}</td>
                        <td>{{ $expense->name }}</td>
                        <td>Rp. {{ number_format($expense->amount, 2, ',', '.') }}</td>
                        <td>
                            <form action="{{ route('expense.destroy', $expense->id) }}" method="post" onsubmit="return confirm('Yakin hapus {{ $expense->name }}?')">
                                {{ method_field('delete') }} 
                                {{ csrf_field() }}
                                <button class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr> 
                    @endforeach
                    <tr>
                        <td colspan="3">Total</td>
                        <td>Rp. {{ number_format($total, 2, ',', '.') }}</td>
                    </tr>
                </tbody> 
            </table>
        </div>
        <div class="box-footer">
            <a href="{{ route('expense.create') }}" class="btn btn-info pull-right">Tambah</a>
        </div>
    </div>
</div>
@endsection