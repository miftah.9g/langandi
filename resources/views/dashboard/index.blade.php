@extends('adminlte::page')

@section('title', 'Langandi:Dashboard')

@section('content_header', "Hai, $user->full_name!")

@section('content')
    <div class="col-md-6">
        <div class="info-box">
            <span class="info-box-icon bg-green">
                <i class="fas fa-sign-in-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Pemasukan bulan ini:</span>
                <span class="info-box-number">Rp. {{ number_format($incomesTotal, 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="info-box">
            <span class="info-box-icon bg-red">
                <i class="fas fa-sign-out-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Pengeluaran bulan ini:</span>
                <span class="info-box-number">Rp. {{ number_format($expensesTotal, 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
@endsection