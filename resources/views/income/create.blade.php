@extends('adminlte::page')

@section('title', 'Langandi:Tambah Pemasukan')

@section('content')
<div class="row col-md-offset-3 col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Pemasukan</h3>
        </div>
        <form action="{{ route('income.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                        @error('name')
                            <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                    <label for="amount" class="col-sm-2 control-label">Jumlah</label>
                    <div class="col-sm-8">
                        <input type="number" name="amount" id="amount" class="form-control" value="{{ old('amount') }}">
                        @error('amount')
                            <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                    <label for="date" class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-8">
                        <input type="date" name="date" id="date" class="form-control" value="{{ old('date') }}">
                        @error('date')
                            <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-info pull-right">Simpan</button>   
            </div>
        </form>
    </div>
</div>
@endsection