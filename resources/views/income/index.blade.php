@extends('adminlte::page')

@section('title', 'Langandi:Daftar Pemasukan')

@section('content')
<div class="row col-md-offset-2 col-md-8">
    @if(session('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
        {{ session('success') }}
    </div>
    @endif

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pemasukan</h3>
            <form action="{{ route('income.index') }}" method="get" class="pull-right">
                <label for="year">Tahun: </label>
                <select name="year" id="year">
                    <option value="">-- Pilih --</option>
                    @for($year = $currentYear; $year >= $farthestYear; $year--)
                    <option value="{{ $year }}" {{ $year == $yearFromInput ? 'selected' : '' }}>{{ $year }}</option>
                    @endfor
                </select>
                <label for="month">Bulan: </label>
                <select name="month" id="month">
                    <option value="">-- Pilih --</option>
                    @for($month = 1; $month <= 12; $month++)
                    <option value="{{ $month }}" {{ $month == $monthFromInput ? 'selected' : '' }}>{{ $month }}</option>
                    @endfor
                </select>
                <button class="btn btn-info">Filter</button> 
            </form>
        </div>
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Action</th>
                    </tr>
                    @foreach($incomes as $income)
                    <tr>
                        <td>{{ $income->date->toDateString() }}</td>
                        <td>{{ $income->name }}</td>
                        <td>Rp. {{ number_format($income->amount, 2, ',', '.') }}</td>
                        <td>
                            <form action="{{ route('income.destroy', $income->id) }}" method="post" onsubmit="return confirm('Yakin hapus {{ $income->name }}?')">
                                {{ method_field('delete') }} 
                                {{ csrf_field() }}
                                <button class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr> 
                    @endforeach
                    <tr>
                        <td colspan="2">Total</td>
                        <td>Rp. {{ number_format($total, 2, ',', '.') }}</td>
                    </tr>
                </tbody> 
            </table>
        </div>
        <div class="box-footer">
            <a href="{{ route('income.create') }}" class="btn btn-info pull-right">Tambah</a>
        </div>
    </div>
</div>
@endsection