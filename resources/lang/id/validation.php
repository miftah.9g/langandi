<?php

return [
    'confirmed' => 'Konfirmasi :attribute tidak cocok.',
    'email'     => 'Email tidak valid.',
    'gt'        => [
        'numeric'   => 'Harus lebih besar dari :value.'
    ],
    'min' => [
        'string'    => 'Minimal :min karakter.'
    ],
    'required'  => 'Harus diisi.',


    'custom'    => [
        'email' => [
            'unique'    => 'Email telah terdaftar.',
        ],
    ],
];