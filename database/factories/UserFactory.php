<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'email'     => $faker->unique()->safeEmail,
        'password'  => bcrypt('secret')
    ];
});
