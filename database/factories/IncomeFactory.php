<?php

use App\Models\Income;

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Income::class, function (Faker $faker) {
    return [
        'name'      => $faker->catchPhrase,
        'amount'    => 10000000,
        'date'      => Carbon::now()    
    ];
});
