<?php

use App\Models\Expense;

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Expense::class, function (Faker $faker) {
    return [
        'name'      => $faker->catchPhrase,
        'amount'    => 10000000,
        'date'      => Carbon::now()    
    ];
});
