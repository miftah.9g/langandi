#! /bin/bash

if [ $# == 1 ]
  then
    php artisan dusk --testdox --filter $1
  else
    php artisan dusk --testdox
fi
