<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable = [
        'name',
        'amount',
        'date',

        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];
}
