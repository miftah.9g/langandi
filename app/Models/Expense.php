<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'amount',
        'category',
        'date',
        'name',

        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];
}
