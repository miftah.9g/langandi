<?php

namespace App\Http\Controllers;

use App\Models\Expense;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if($user == null) {
            return redirect('/login');
        }

        $expenses = $this->userExpensesBuilder(
            $user, 
            $request->input('year'), 
            $request->input('month'), 
            $request->input('category')
        )->get();
        $total = $expenses->sum('amount');

        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;
        $farthestYear = $currentYear - 10;
        $categories = ['none', 'rutin', 'hiburan', 'investasi', 'pendidikan', 'kesehatan'];

        return view('expense.index', [
            'expenses'          => $expenses,
            'total'             => $total,
            'currentYear'       => $currentYear,
            'farthestYear'      => $farthestYear,
            'yearFromInput'     => $request->input('year', $currentYear),
            'monthFromInput'    => $request->input('month', $currentMonth),
            'categories'        => $categories,
            'categoryFromInput' => $request->input('category')
        ]);
    }

    public function create()
    {
        if(Auth::user() == null) {
            return redirect(route('auth.login'));
        }

        $categories = ['rutin', 'hiburan', 'investasi', 'pendidikan', 'kesehatan'];

        return view('expense.create', [
            'categories'    => $categories
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'amount'    => 'required|numeric|gt:0',
            'date'      => 'required'
        ]);

        $user = Auth::user();

        Expense::create([
            'amount'    => $request->input('amount'),
            'category'  => $request->input('category'),
            'date'      => $request->input('date'),
            'name'      => $request->input('name'),

            'user_id'   => $user->id
        ]);        

        return redirect(route('expense.index'))
                    ->with('success', 'Pengeluaran berhasil disimpan.');
    }

    public function destroy($id)
    {
        if(Auth::user() == null) {
            return redirect('/login');
        }

        $expense = Auth::user()->expenses()->find($id);

        if($expense) {
            $expense->delete();
        }

        return redirect(route('expense.index'))
                    ->with('success', 'Pengeluaran berhasil dihapus.');
    }

    private function userExpensesBuilder($user, $yearFromInput, $monthFromInput, $categoryFromInput)
    {
        $userExpensesBuilder = $user->expenses();

        if($yearFromInput && $monthFromInput) {
            $userExpensesBuilder->where('date', '>=', Carbon::createFromDate($yearFromInput, $monthFromInput, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate($yearFromInput, $monthFromInput, 1)->endOfMonth());
        }
        elseif ($yearFromInput) {
            $userExpensesBuilder->where('date', '>=', Carbon::createFromDate($yearFromInput)->startOfYear())
                               ->where('date', '<=', Carbon::createFromDate($yearFromInput)->endOfYear());
        } 
        elseif ($monthFromInput) {
            $userExpensesBuilder->where('date', '>=', Carbon::createFromDate(null, $monthFromInput, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate(null, $monthFromInput, 1)->endOfMonth());
        } else {
            $currentYear = Carbon::now()->year;
            $currentMonth = Carbon::now()->month;

            $userExpensesBuilder->where('date', '>=', Carbon::createFromDate($currentYear, $currentMonth, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate($currentYear, $currentMonth, 1)->endOfMonth());
        }

        if ($categoryFromInput) {
            if ($categoryFromInput == 'none') { $categoryFromInput = null; }  

            $userExpensesBuilder->where('category', $categoryFromInput);
        }

        $userExpensesBuilder->orderBy('date');

        return $userExpensesBuilder;
    }
}
