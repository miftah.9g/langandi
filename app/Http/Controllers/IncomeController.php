<?php

namespace App\Http\Controllers;

use App\Models\Income;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IncomeController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if($user == null) {
            return redirect('/login');
        }

        $incomes = $this->userIncomesBuilder($user, $request->input('year'), $request->input('month'))->get();
        $total = $incomes->sum('amount');

        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;
        $farthestYear = $currentYear - 10;

        return view('income.index', [
            'incomes'           => $incomes,
            'total'             => $total,
            'currentYear'       => $currentYear,
            'farthestYear'      => $farthestYear,
            'yearFromInput'     => $request->input('year', $currentYear),
            'monthFromInput'    => $request->input('month', $currentMonth),
        ]);
    }

    public function create()
    {
        if(Auth::user() == null) {
            return redirect('/login');
        }

        return view('income.create');
    }

    public function store(Request $request)
    {
        if(Auth::user() == null) {
            return redirect('/login');
        }

        $request->validate([
            'name'      => 'required',
            'amount'    => 'required|numeric|gt:0',
            'date'      => 'required'
        ]);

        $user = Auth::user();

        Income::create([
            'name'      => $request->input('name'),
            'amount'    => $request->input('amount'),
            'date'      => $request->input('date'),
            'user_id'   => $user->id,
        ]);        

        return redirect(route('income.index'))
                    ->with('success', 'Pemasukan berhasil disimpan.');
    }

    public function destroy($id)
    {
        if(Auth::user() == null) {
            return redirect('/login');
        }

        $income = Auth::user()->incomes()->where('id', $id);

        if ($income) {
            $income->delete();
        }

        return redirect(route('income.index'))
                    ->with('success', 'Pemasukan berhasil dihapus.');
    }

    private function userIncomesBuilder($user, $yearFromInput, $monthFromInput)
    {
        $userIncomesBuilder = $user->incomes();

        if($yearFromInput && $monthFromInput) {
            $userIncomesBuilder->where('date', '>=', Carbon::createFromDate($yearFromInput, $monthFromInput, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate($yearFromInput, $monthFromInput, 1)->endOfMonth());
        }
        elseif ($yearFromInput) {
            $userIncomesBuilder->where('date', '>=', Carbon::createFromDate($yearFromInput)->startOfYear())
                               ->where('date', '<=', Carbon::createFromDate($yearFromInput)->endOfYear());
        } 
        elseif ($monthFromInput) {
            $userIncomesBuilder->where('date', '>=', Carbon::createFromDate(null, $monthFromInput, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate(null, $monthFromInput, 1)->endOfMonth());
        } else {
            $currentYear = Carbon::now()->year;
            $currentMonth = Carbon::now()->month;

            $userIncomesBuilder->where('date', '>=', Carbon::createFromDate($currentYear, $currentMonth, 1)->startOfMonth())
                               ->where('date', '<=', Carbon::createFromDate($currentYear, $currentMonth, 1)->endOfMonth());
        }

        $userIncomesBuilder->orderBy('date');

        return $userIncomesBuilder;
    }
}
