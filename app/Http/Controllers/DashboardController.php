<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index() 
    {
        $user = Auth::user();

        if($user == null) {
            return redirect(route('auth.login'));
        }

        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;

        $incomesTotal = $user->incomes()
                             ->where('date', '>=', Carbon::createFromDate($currentYear, $currentMonth)->startOfMonth())
                             ->where('date', '<=', Carbon::createFromDate($currentYear, $currentMonth)->endOfMonth())
                             ->get()->sum('amount');

        $expensesTotal = $user->expenses()
                             ->where('date', '>=', Carbon::createFromDate($currentYear, $currentMonth)->startOfMonth())
                             ->where('date', '<=', Carbon::createFromDate($currentYear, $currentMonth)->endOfMonth())
                             ->get()->sum('amount');

        return view('dashboard.index', [
            'user'          => $user,
            'incomesTotal'  => $incomesTotal,
            'expensesTotal'  => $expensesTotal
        ]);
    }
}
