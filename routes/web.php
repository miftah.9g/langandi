<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('dashboard.index'));
});

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
Route::post('/register', 'Auth\RegisterController@register')->name('auth.register');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::post('/login', 'Auth\LoginController@login')->name('auth.login');
Route::delete('/logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

Route::get('/pemasukan/tambah', 'IncomeController@create')->name('income.create');
Route::delete('/pemasukan/{id}', 'IncomeController@destroy')->name('income.destroy');
Route::get('/pemasukan', 'IncomeController@index')->name('income.index');
Route::post('/pemasukan', 'IncomeController@store')->name('income.store');

Route::get('/pengeluaran/tambah', 'ExpenseController@create')->name('expense.create');
Route::delete('/pengeluaran/{id}', 'ExpenseController@destroy')->name('expense.destroy');
Route::get('/pengeluaran', 'ExpenseController@index')->name('expense.index');
Route::post('/pengeluaran', 'ExpenseController@store')->name('expense.store');