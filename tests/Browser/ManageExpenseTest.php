<?php

namespace Tests\Browser;

use App\Models\Expense;
use App\Models\User;

use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ManageExpenseTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testOpenIndexExpensePageWhenUserIsNotLoggedIn()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pengeluaran')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenIndexExpensePage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $currentYear = Carbon::now()->year;
            $currentMonth = Carbon::now()->month;
            $farthestYear = $currentYear - 10;

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->assertPathIs('/pengeluaran')
                        ->assertTitle('Langandi:Daftar Pengeluaran')
                        ->assertSee('Daftar Pengeluaran')
                        ->assertSee('Kategori:')
                        ->assertSelectHasOptions('category', [null, 'none', 'rutin', 'hiburan', 'investasi', 'pendidikan', 'kesehatan'])
                        ->assertSee('Tahun:')
                        ->assertSelected('year', $currentYear)
                        ->assertSelectHasOption('year', null)
                        ->assertSelectHasOption('year', $currentYear)
                        ->assertSelectHasOption('year', $farthestYear)
                        ->assertSee('Bulan:')
                        ->assertSelectHasOptions('month', [null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
                        ->assertSelected('month', $currentMonth)
                        ->assertSee('Tanggal')
                        ->assertSee('Kategori')
                        ->assertSee('Nama')
                        ->assertSee('Jumlah')
                        ->assertSee('Total')
                        ->assertDontSee('Berhasil!')
                        ->assertDontSee('Pengeluran berhasil disimpan.');
        });
    }

    public function testGoToAddExpensePageFromIndexExpensePage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->clickLink('Tambah')
                        ->assertPathIs('/pengeluaran/tambah');
        });
    }
    
    public function testOpenAddExpensePageWhenUserIsNotLoggedIn() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pengeluaran/tambah')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenAddExpensePage() 
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->assertTitle('Langandi:Tambah Pengeluaran')
                        ->assertSee('Tambah Pengeluaran')
                        ->assertSee('Nama')
                        ->assertSee('Jumlah')
                        ->assertPresent('input[name="amount"][type="number"]')
                        ->assertSee('Tanggal')
                        ->assertPresent('input[name="date"][type="date"]');
        });
    }

    public function testAddExpense()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', 10000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran')
                        ->assertSee('2019-08-25')
                        ->assertSee('Belanja bulan Agustus')
                        ->assertSee('Rp. 10.000.000,00')
                        ->assertSee('Berhasil!')
                        ->assertSee('Pengeluaran berhasil disimpan.');
        });
    }
    
    public function testAddExpenseWithBlankName()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('amount', 10000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="amount"][value="10000000"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddExpenseWithBlankAmount()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="name"][value="Belanja bulan Agustus"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddExpenseWithAmountZero()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', '0')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran/tambah')
                        ->assertSee('Harus lebih besar dari 0.')
                        ->assertPresent('input[name="name"][value="Belanja bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="0"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddExpenseWithAmountLessThanZero()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', '-1')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran/tambah')
                        ->assertSee('Harus lebih besar dari 0.')
                        ->assertPresent('input[name="name"][value="Belanja bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="-1"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddExpenseWithBlankDate()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', 10000000)
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="name"][value="Belanja bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="10000000"]');
        });
    }

    public function testAddExpenseWithCategory()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $expectedCategories = ['rutin', 'hiburan', 'investasi', 'pendidikan', 'kesehatan'];

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', 10000000)
                        ->keys('input[name="date"]', '25082019')
                        ->assertSelectHasOptions('category', $expectedCategories)
                        ->select('category', 'rutin')
                        ->press('Simpan')
                        ->assertPathIs('/pengeluaran')
                        ->assertSee('2019-08-25')
                        ->assertSee('Rutin')
                        ->assertSee('Belanja bulan Agustus')
                        ->assertSee('Rp. 10.000.000,00')
                        ->assertSee('Berhasil!')
                        ->assertSee('Pengeluaran berhasil disimpan.');
        });
    }

    public function testOpenIndexExpensePageWhenUserHasExpenses()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $lastYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $currentMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->assertDontSee('00:00:00')
                        ->assertSee($currentMonthExpense->name)
                        ->assertSee($currentMonthExpense->date->toDateString())
                        ->assertSee(number_format($currentMonthExpense->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearExpense->name)
                        ->assertDontSee($lastYearExpense->date->toDateString())
                        ->assertDontSee($lastMonthExpense->name)
                        ->assertDontSee($lastMonthExpense->date->toDateString())
                        ->assertDontSee($nextMonthExpense->name)
                        ->assertDontSee($nextMonthExpense->date->toDateString());
        });
    }

    public function testOpenIndexExpensePageWhenUserHasNoExpenseButOtherUserHasExpense()
    {
        $this->browse(function (Browser $browser1, Browser $browser2) {
            $user1 = factory(User::class)->create();
            $user2 = factory(User::class)->create();

            DuskTestCase::login($user1, $browser1)
                        ->visit('/pengeluaran/tambah')
                        ->type('name', 'Belanja bulan Agustus')
                        ->type('amount', 15000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan');

            DuskTestCase::login($user2, $browser2)
                        ->visit('/pengeluaran')
                        ->assertDontSee('2019-08-25')
                        ->assertDontSee('Belanja bulan Agustus')
                        ->assertDontSee('Rp. 15.000.000,00')
                        ->assertDontSee('Rp. 15.000.000,00');
        });
    }

    public function testGoToDashboardFromExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran/tambah')
                        ->clickLink('Langandi')
                        ->assertPathIs('/dashboard');
        });
    }

    public function testFilterByYearInExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()]);
            $currentYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addYear()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->select('year', $lastYearExpense->date->year)
                        ->press('Filter')
                        ->assertSelected('year', $lastYearExpense->date->year)
                        ->assertSee($lastYearExpense->name)
                        ->assertSee($lastYearExpense->date->toDateString())
                        ->assertSee(number_format($lastYearExpense->amount, 2, ',', '.'))
                        ->assertDontSee($currentYearExpense->name)
                        ->assertDontSee($currentYearExpense->date->toDateString())
                        ->assertDontSee($nextYearExpense->name)
                        ->assertDontSee($nextYearExpense->date->toDateString());
        });
    }

    public function testFilterByMonthInExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $lastYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $currentMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->select('month', $lastMonthExpense->date->month)
                        ->press('Filter')
                        ->assertSelected('month', $lastMonthExpense->date->month)
                        ->assertSee($lastMonthExpense->name)
                        ->assertSee($lastMonthExpense->date->toDateString())
                        ->assertSee(number_format($lastMonthExpense->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearExpense->name)
                        ->assertDontSee($lastYearExpense->date->toDateString())
                        ->assertDontSee($currentMonthExpense->name)
                        ->assertDontSee($currentMonthExpense->date->toDateString())
                        ->assertDontSee($nextMonthExpense->name)
                        ->assertDontSee($nextMonthExpense->date->toDateString());
        });
    }

    public function testFilterByYearAndMonthInExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastYearExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $lastYearExpenseDifferentMonth = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonths(3)]);
            $lastMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $currentMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->select('year', $lastYearExpense->date->year)
                        ->select('month', $lastYearExpense->date->month)
                        ->press('Filter')
                        ->assertSelected('year', $lastYearExpense->date->year)
                        ->assertSelected('month', $lastYearExpense->date->month)
                        ->assertSee($lastYearExpense->name)
                        ->assertSee($lastYearExpense->date->toDateString())
                        ->assertSee(number_format($lastYearExpense->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearExpenseDifferentMonth->name)
                        ->assertDontSee($lastYearExpenseDifferentMonth->date->toDateString())
                        ->assertDontSee($lastMonthExpense->name)
                        ->assertDontSee($lastMonthExpense->date->toDateString())
                        ->assertDontSee($currentMonthExpense->name)
                        ->assertDontSee($currentMonthExpense->date->toDateString())
                        ->assertDontSee($nextMonthExpense->name)
                        ->assertDontSee($nextMonthExpense->date->toDateString());
        });       
    }

    public function testFilterByCategoryInExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $rutinExpense = factory(Expense::class)->create(['user_id' => $user->id, 'category' => 'rutin']);
            $pendidikanExpense = factory(Expense::class)->create(['user_id' => $user->id, 'category' => 'pendidikan']);
            $noCategoryExpense = factory(Expense::class)->create(['user_id' => $user->id]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->select('category', $rutinExpense->category)
                        ->press('Filter')
                        ->assertSelected('category', $rutinExpense->category)
                        ->assertSee($rutinExpense->name)
                        ->assertSee(ucfirst($rutinExpense->category))
                        ->assertSee($rutinExpense->date->toDateString())
                        ->assertSee(number_format($rutinExpense->amount, 2, ',', '.'))
                        ->assertDontSee($pendidikanExpense->name)
                        ->assertDontSee($noCategoryExpense->name);
        });       
    }

    public function testFilterByCategoryNoneInExpenseIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $rutinExpense = factory(Expense::class)->create(['user_id' => $user->id, 'category' => 'rutin']);
            $pendidikanExpense = factory(Expense::class)->create(['user_id' => $user->id, 'category' => 'pendidikan']);
            $noCategoryExpense = factory(Expense::class)->create(['user_id' => $user->id]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->select('category', 'none')
                        ->press('Filter')
                        ->assertSelected('category', 'none')
                        ->assertSee($noCategoryExpense->name)
                        ->assertSee('-')
                        ->assertSee($noCategoryExpense->date->toDateString())
                        ->assertSee(number_format($noCategoryExpense->amount, 2, ',', '.'))
                        ->assertDontSee($rutinExpense->name)
                        ->assertDontSee($pendidikanExpense->name);
        });       
    }

    public function testDeleteExpense()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $currentMonthExpenses = factory(Expense::class, 2)->create(['user_id' => $user->id, 'date' => Carbon::now()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pengeluaran')
                        ->press('Hapus')
                        ->acceptDialog()
                        ->assertPathIs('/pengeluaran')
                        ->assertSee($currentMonthExpenses->last()->name)
                        ->assertDontSee($currentMonthExpenses->first()->name)
                        ->assertSee('Pengeluaran berhasil dihapus.');
        });
    }
}
