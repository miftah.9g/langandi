<?php

namespace Tests\Browser;

use App\Models\User;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testOpenLoginPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Masuk')
                    ->assertPresent("input[name='email'][placeholder='Email']")
                    ->assertPresent("input[name='password'][placeholder='Password']")
                    ->assertSee('Belum punya akun?');
        });
    }

    public function testGoToRegisterPageFromLoginPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->clickLink('Daftar')
                    ->assertPathIs('/register');
        });
    }

    public function testOpenLoginPageWhenAlreadyLoggedIn()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Masuk')
                    ->visit('/login')
                    ->assertPathIs('/dashboard');
        });
    }

    public function testLoginWithValidEmailAndPassword()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Masuk')
                    ->assertPathIs('/dashboard');
        });
    }

    public function testLoginWithWrongPassword()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'felany ganteng')
                    ->press('Masuk')
                    ->assertSee('Password salah, harap coba kembali.')
                    ->assertPathIs('/login')
                    ->assertPresent("input[name='email'][value='{$user->email}']");
        });
    }

    public function testLoginWithBlankPassword()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', '    ')
                    ->press('Masuk')
                    ->assertSee('Harus diisi')
                    ->assertPathIs('/login')
                    ->assertPresent("input[name='email'][value='{$user->email}']");
        });
    }

    public function testLoginWithoutPassword()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->press('Masuk')
                    ->assertSee('Harus diisi')
                    ->assertPathIs('/login')
                    ->assertPresent("input[name='email'][value='{$user->email}']");
        });
    }

    public function testLoginWithUnregisteredEmail()
    {
        $this->browse(function (Browser $browser) {
            $email = 'test@langandi.com';

            $browser->visit('/login')
                    ->type('email', $email)
                    ->type('password', 'secret')
                    ->press('Masuk')
                    ->assertSee('Email tidak terdaftar, harap coba kembali')
                    ->assertPathIs('/login')
                    ->assertPresent("input[name='email'][value='{$email}']");
        });
    }

    public function testLoginWithBlankEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', '    ')
                    ->type('password', 'secret')
                    ->press('Masuk')
                    ->assertSee('Harus diisi')
                    ->assertPathIs('/login');
        });
    }

    public function testLoginWithoutEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('password', 'secret')
                    ->press('Masuk')
                    ->assertSee('Harus diisi')
                    ->assertPathIs('/login');
        });
    }
}