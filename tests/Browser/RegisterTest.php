<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testOpenRegisterPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->assertPresent('input[name="full_name"][type="text"][placeholder="Nama Lengkap"]')
                    ->assertPresent('input[name="email"][type="text"][placeholder="Email"]')
                    ->assertPresent('input[name="password"][type="password"][placeholder="Password"]')
                    ->assertPresent('input[name="password_confirmation"][type="password"][placeholder="Konfirmasi Password"]')
                    ->assertSee('Daftar');
        });
    }

    public function testGoToLoginPageFromRegisterPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->assertSee('Sudah punya akun?')
                    ->clickLink('Masuk')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenRegisterPageWhenAlreadyLoggedIn()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')
                    ->assertPathIs('/dashboard')
                    ->visit('/register')
                    ->assertPathIs('/dashboard');
        });
    }

    public function testRegisterWithValidEmailAndPassword()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/dashboard')
                    ->assertSee('Hai, ' . $fullName . '!');
        });
    }

    public function testRegisterWithARegisteredEmail()
    {
        $this->browse(function (Browser $browser1, Browser $browser2) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';

            $browser1->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar');

            $browser2->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Email telah terdaftar.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithInvalidEmail()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'invalid_email';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Email tidak valid.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithBlankEmail()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = '     ';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Harus diisi.')
                    ->assertPresent("input[name='full_name'][value='$fullName']");
        });
    }

    public function testRegisterWithEmptyEmail()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Harus diisi.')
                    ->assertPresent("input[name='full_name'][value='$fullName']");
        });
    }

    public function testRegisterWithLessThan8CharactersPassword()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1234567';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Minimal 8 karakter.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithBlankPassword()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '       ';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Harus diisi.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithEmptyPassword()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Harus diisi.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithMismatchPasswordConfirmation()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';
            $passwordConfirmation = 'abcdef';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $passwordConfirmation)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Konfirmasi password tidak cocok.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithBlankPasswordConfirmation()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';
            $passwordConfirmation = '     ';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->type('password_confirmation', $passwordConfirmation)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Konfirmasi password tidak cocok.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }

    public function testRegisterWithEmptyPasswordConfirmation()
    {
        $this->browse(function (Browser $browser) {
            $fullName = 'Akun Test';
            $email = 'test@langandi.com';
            $password = '1029384756';

            $browser->visit('/register')
                    ->type('full_name', $fullName)
                    ->type('email', $email)
                    ->type('password', $password)
                    ->press('Daftar')

                    ->assertPathIs('/register')
                    ->assertSee('Konfirmasi password tidak cocok.')
                    ->assertPresent("input[name='full_name'][value='$fullName']")
                    ->assertPresent("input[name='email'][value='$email']");
        });
    }
}
