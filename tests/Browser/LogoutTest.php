<?php

namespace Tests\Browser;

use App\Models\User;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class LogoutTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testLogout()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Masuk')
                    
                    ->clickLink('Log Out')
                    ->assertPathIs('/login')

                    ->visit('/dashboard')
                    ->assertPathIs('/login');
        });
    }
}