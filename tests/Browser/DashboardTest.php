<?php

namespace Tests\Browser;

use App\Models\Expense;
use App\Models\Income;
use App\Models\User;

use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DashboardTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testOpenDashboardPageWhenUserIsNotLoggedIn()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/dashboard')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenDashboardPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->assertTitle('Langandi:Dashboard')
                        ->assertSee("Hai, $user->full_name!");
        });
    }

    public function testGoToIndexIncomePageFromDashboard()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->clickLink('Pemasukan')
                        ->assertPathIs('/pemasukan');
        });
    }

    public function testGoToIndexExpensePageFromDashboard()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->clickLink('Pengeluaran')
                        ->assertPathIs('/pengeluaran');
        });
    }

    public function testSeeCurrentMonthTotalIncomeOnDashboardPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $currentMonthIncomes = factory(Income::class, 2)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);
            $incomesTotal = $currentMonthIncomes->sum('amount');

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->assertSee('PEMASUKAN BULAN INI:')
                        ->assertSee('Rp. ' . number_format($incomesTotal, 2, ',', '.'));
        });
    }

    public function testSeeTotalIncomeOnDashboardPageWhenUserHasNoIncome()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->assertSee('PEMASUKAN BULAN INI:')
                        ->assertSee('Rp. ' . number_format(0, 2, ',', '.'));
        });
    }

    public function testSeeCurrentMonthTotalExpenseOnDashboardPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $currentMonthExpenses = factory(Expense::class, 2)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthExpense = factory(Expense::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);
            $expensesTotal = $currentMonthExpenses->sum('amount');

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->assertSee('PENGELUARAN BULAN INI:')
                        ->assertSee('Rp. ' . number_format($expensesTotal, 2, ',', '.'));
        });
    }

    public function testSeeTotalExpenseOnDashboardPageWhenUserHasNoExpense()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/dashboard')
                        ->assertSee('PENGELUARAN BULAN INI:')
                        ->assertSee('Rp. ' . number_format(0, 2, ',', '.'));
        });
    }

    public function testOpenRootPath()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/')
                        ->assertPathIs('/dashboard');
        }); 
    }
}
