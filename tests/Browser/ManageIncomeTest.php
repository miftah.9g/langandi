<?php

namespace Tests\Browser;

use App\Models\Income;
use App\Models\User;

use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ManageIncomeTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testOpenIndexIncomePageWhenUserIsNotLoggedIn()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pemasukan')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenIndexIncomePage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $currentYear = Carbon::now()->year;
            $currentMonth = Carbon::now()->month;
            $farthestYear = $currentYear - 10;

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->assertPathIs('/pemasukan')
                        ->assertTitle('Langandi:Daftar Pemasukan')
                        ->assertSee('Daftar Pemasukan')
                        ->assertSee('Tahun:')
                        ->assertSelected('year', $currentYear)
                        ->assertSelectHasOption('year', null)
                        ->assertSelectHasOption('year', $currentYear)
                        ->assertSelectHasOption('year', $farthestYear)
                        ->assertSee('Bulan:')
                        ->assertSelectHasOptions('month', [null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
                        ->assertSelected('month', $currentMonth)
                        ->assertSee('Tanggal')
                        ->assertSee('Nama')
                        ->assertSee('Jumlah')
                        ->assertSee('Total')
                        ->assertDontSee('Berhasil!')
                        ->assertDontSee('Pemasukan berhasil disimpan.');
        });
    }

    public function testGoToAddIncomePageFromIndexIncomePage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->clickLink('Tambah')
                        ->assertPathIs('/pemasukan/tambah');
        });
    }

    public function testOpenAddIncomePageWhenUserIsNotLoggedIn() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/pemasukan/tambah')
                    ->assertPathIs('/login');
        });
    }

    public function testOpenAddIncomePage() 
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->assertTitle('Langandi:Tambah Pemasukan')
                        ->assertSee('Tambah Pemasukan')
                        ->assertSee('Nama')
                        ->assertSee('Jumlah')
                        ->assertPresent('input[name="amount"][type="number"]')
                        ->assertSee('Tanggal')
                        ->assertPresent('input[name="date"][type="date"]');
        });
    }

    public function testAddIncome()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->type('amount', 10000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan')
                        ->assertSee('2019-08-25')
                        ->assertSee('Gaji bulan Agustus')
                        ->assertSee('Rp. 10.000.000,00')
                        ->assertSee('Berhasil!')
                        ->assertSee('Pemasukan berhasil disimpan.');
        });
    }

    public function testAddIncomeWithBlankName()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('amount', 10000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="amount"][value="10000000"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddIncomeWithBlankAmount()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="name"][value="Gaji bulan Agustus"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddIncomeWithAmountZero()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->type('amount', '0')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan/tambah')
                        ->assertSee('Harus lebih besar dari 0.')
                        ->assertPresent('input[name="name"][value="Gaji bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="0"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddIncomeWithAmountLessThanZero()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->type('amount', '-1')
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan/tambah')
                        ->assertSee('Harus lebih besar dari 0.')
                        ->assertPresent('input[name="name"][value="Gaji bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="-1"]')
                        ->assertPresent('input[name="date"][value="2019-08-25"]');
        });
    }

    public function testAddIncomeWithBlankDate()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->type('amount', 10000000)
                        ->press('Simpan')
                        ->assertPathIs('/pemasukan/tambah')
                        ->assertSee('Harus diisi.')
                        ->assertPresent('input[name="name"][value="Gaji bulan Agustus"]')
                        ->assertPresent('input[name="amount"][value="10000000"]');
        });
    }

    public function testOpenIndexIncomePageWhenUserHasIncomes()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $lastYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $currentMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->assertDontSee('00:00:00')
                        ->assertSee($currentMonthIncome->name)
                        ->assertSee($currentMonthIncome->date->toDateString())
                        ->assertSee(number_format($currentMonthIncome->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearIncome->name)
                        ->assertDontSee($lastYearIncome->date->toDateString())
                        ->assertDontSee($lastMonthIncome->name)
                        ->assertDontSee($lastMonthIncome->date->toDateString())
                        ->assertDontSee($nextMonthIncome->name)
                        ->assertDontSee($nextMonthIncome->date->toDateString());
        });
    }

    public function testOpenIndexIncomePageWhenUserHasNoIncomeButOtherUserHasIncome()
    {
        $this->browse(function (Browser $browser1, Browser $browser2) {
            $user1 = factory(User::class)->create();
            $user2 = factory(User::class)->create();

            DuskTestCase::login($user1, $browser1)
                        ->visit('/pemasukan/tambah')
                        ->type('name', 'Gaji bulan Agustus')
                        ->type('amount', 15000000)
                        ->keys('input[name="date"]', '25082019')
                        ->press('Simpan');

            DuskTestCase::login($user2, $browser2)
                        ->visit('/pemasukan')
                        ->assertDontSee('2019-08-25')
                        ->assertDontSee('Gaji bulan Agustus')
                        ->assertDontSee('Rp. 15.000.000,00')
                        ->assertDontSee('2019-08-26');
        });
    }

    public function testGoToDashboardFromIncomeIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan/tambah')
                        ->clickLink('Langandi')
                        ->assertPathIs('/dashboard');
        });
    }

    public function testFilterByYearInIncomeIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()]);
            $currentYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addYear()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->select('year', $lastYearIncome->date->year)
                        ->press('Filter')
                        ->assertSelected('year', $lastYearIncome->date->year)
                        ->assertSee($lastYearIncome->name)
                        ->assertSee($lastYearIncome->date->toDateString())
                        ->assertSee(number_format($lastYearIncome->amount, 2, ',', '.'))
                        ->assertDontSee($currentYearIncome->name)
                        ->assertDontSee($currentYearIncome->date->toDateString())
                        ->assertDontSee($nextYearIncome->name)
                        ->assertDontSee($nextYearIncome->date->toDateString());
        });
    }

    public function testFilterByMonthInIncomeIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $lastYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $currentMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->select('month', $lastMonthIncome->date->month)
                        ->press('Filter')
                        ->assertSelected('month', $lastMonthIncome->date->month)
                        ->assertSee($lastMonthIncome->name)
                        ->assertSee($lastMonthIncome->date->toDateString())
                        ->assertSee(number_format($lastMonthIncome->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearIncome->name)
                        ->assertDontSee($lastYearIncome->date->toDateString())
                        ->assertDontSee($currentMonthIncome->name)
                        ->assertDontSee($currentMonthIncome->date->toDateString())
                        ->assertDontSee($nextMonthIncome->name)
                        ->assertDontSee($nextMonthIncome->date->toDateString());
        });
    }

    public function testFilterByYearAndMonthInIncomeIndexPage()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $lastYearIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonth()]);
            $lastYearIncomeDifferentMonth = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subYear()->subMonths(3)]);
            $lastMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->subMonth()]);
            $currentMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()]);
            $nextMonthIncome = factory(Income::class)->create(['user_id' => $user->id, 'date' => Carbon::now()->addMonth()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->select('year', $lastYearIncome->date->year)
                        ->select('month', $lastYearIncome->date->month)
                        ->press('Filter')
                        ->assertSelected('year', $lastYearIncome->date->year)
                        ->assertSelected('month', $lastYearIncome->date->month)
                        ->assertSee($lastYearIncome->name)
                        ->assertSee($lastYearIncome->date->toDateString())
                        ->assertSee(number_format($lastYearIncome->amount, 2, ',', '.'))
                        ->assertDontSee($lastYearIncomeDifferentMonth->name)
                        ->assertDontSee($lastYearIncomeDifferentMonth->date->toDateString())
                        ->assertDontSee($lastMonthIncome->name)
                        ->assertDontSee($lastMonthIncome->date->toDateString())
                        ->assertDontSee($currentMonthIncome->name)
                        ->assertDontSee($currentMonthIncome->date->toDateString())
                        ->assertDontSee($nextMonthIncome->name)
                        ->assertDontSee($nextMonthIncome->date->toDateString());
        });       
    }

    public function testDeleteIncome()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $currentMonthIncomes = factory(Income::class, 2)->create(['user_id' => $user->id, 'date' => Carbon::now()]);

            DuskTestCase::login($user, $browser)
                        ->visit('/pemasukan')
                        ->press('Hapus')
                        ->acceptDialog()
                        ->assertPathIs('/pemasukan')
                        ->assertSee($currentMonthIncomes->last()->name)
                        ->assertDontSee($currentMonthIncomes->first()->name)
                        ->assertSee('Pemasukan berhasil dihapus.');
        });

    }
}
